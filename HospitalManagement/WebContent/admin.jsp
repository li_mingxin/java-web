<%@page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<title>管理员操作界面</title>
	<link rel="stylesheet" type="text/css" href="css/user&admin.css">
	<link rel="icon" type="image/x-ico" href="images/stu.ico">
</head>
	
<body>
	<%
		//获取登录成功的用户信息
		User user = (User) session.getAttribute("admin");
		//判断用户是否登录
		if(user != null){
	%>
	<header>
		<div class="title">
			<span>管理员操作界面</span>
		</div>
		<nav>
			<div class="userinfo">
				<ul>
					<li><%=user.getUsername() %></li>
					<li><%=user.getLevel() %></li>
					<li><a href="UserExitServlet">退出登录</a></li>
					<li><a href="login.html">返回首页</a></li>
				</ul>
			</div>
		</nav>
	</header>
	
	<main>
		<%
		}else{
			response.sendRedirect("login.html");
		}
		%>
		<div class="container">
			<div class="select">
				<h3>请选择操作</h3>
				<ul id="accordion" class="accordion">
					<li>
						<div id="user-info" class="link"></i>用户信息管理</div>
						<ul class="submenu">
							<li><a onclick="query_all('user')">查看所有用户</a></li>
							<li><a onclick="show_insert_user()">新增用户信息</a></li>	
							<li><a onclick="show_delete('user')">删除指定用户</a></li>				
							<li><a onclick="show_alter('user')">修改用户信息</a></li>							
						</ul>
					</li>
					<li>
						<div class="link"></i>科系信息管理</div>
						<ul class="submenu">
							<li><a onclick="query_all('department')">查看所有科系</a></li>
							<li><a onclick="show_insert_department()">新增科系信息</a></li>
							<li><a onclick="show_delete('department')">删除指定科系</a></li>		
							<li><a onclick="show_alter('department')">修改科系信息</a></li>	
						</ul>
					</li>
					<li>
						<div class="link">病房信息管理</div>
						<ul class="submenu">
							<li><a onclick="query_all('class')">查看所有病房</a></li>
							<li><a onclick="show_insert_class()">新增病房信息</a></li>
							<li><a onclick="show_delete('class')">删除指定病房</a></li>				
							<li><a onclick="show_alter('class')">修改病房信息</a></li>	
						</ul>
					</li>
					<li>
						<div class="link">病人信息管理</div>
						<ul class="submenu">
							<li><a  onclick="query_all('student')">查看所有病人</a></li>
							<li><a onclick="show_insert_student()">新增病人信息</a></li>	
							<li><a onclick="show_delete('student')">删除指定病人</a></li>					
							<li><a onclick="show_alter('student')">修改病人信息</a></li>	
						</ul>
					</li>
					<li>
						<div class="link">医生类型管理</div>
						<ul class="submenu">
							<li><a onclick="query_all('course')">查看所有医生</a></li>
							<li><a onclick="show_insert_course()">新增医生信息</a></li>	
							<li><a onclick="show_delete('course')">删除医生信息</a></li>
							<li><a onclick="show_alter('course')">修改医生信息</a></li>	
						</ul>
					</li>
					<li>
						<div class="link">病人手术管理</div>
						<ul class="submenu">
							<li><a  onclick="query_all('sc')">查看病人手术安排</a></li>
							<li><a onclick="show_insert_sc()">新增病人手术安排</a></li>	
							<li><a onclick="show_delete('sc')">删除病人手术安排</a></li>					
							<li><a onclick="show_alter('sc')">修改病人手术安排</a></li>	
						</ul>
					</li>
				</ul>
				</div>
	
				<div id="result" class="result">
					<p class="welcome">欢迎使用医院信息管理系统！</p>
				</div>
			</div>
		</div>
	</main>
	


	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/admin.js"></script>
</body>
</html>