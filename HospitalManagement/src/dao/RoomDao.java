package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import utils.DBUtils;
import model.Room;

public class RoomDao {
	// 获取所有病房的信息，用ArrayList返回
	public ArrayList<Room> query_all_class() {
		Connection conn = DBUtils.getConnection();
		//查询语句
		String sql = "select Clno,Clname,department.Dname from class,department where class.Dno=department.Dno;";
		ArrayList<Room> results = new ArrayList<Room>();
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Room temp = new Room();
				temp.setClno(rs.getString("clno"));
				temp.setClname(rs.getString("clname"));
				temp.setDname(rs.getString("dname"));
				results.add(temp);
			}
			// 关闭资源
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return results;
	}
	// 插入病房信息，返回一个int值表示状态,1：成功，0失败
	public int insert_class(String clno, String clname, String dname) {
		Connection conn = DBUtils.getConnection();
		String sql = "insert into class values(?,?,?);";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, clno);
			ps.setString(2, clname);
			ps.setString(3, dname);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}
	// 删除病房信息，返回一个int值表示状态,1：成功，0失败
	public int delete_class(String clno) {
		Connection conn = DBUtils.getConnection();
		String sql = "delete from class where clno = ?;";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, clno);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}
	// 修改病房信息，返回一个int值表示状态,1：成功，0失败
	public int alter_class(String clno, String after_clno, String after_clname, String after_dname) {
		Connection conn = DBUtils.getConnection();
		String sql = "update class set clno = ?,clname = ?,dno = ? where clno = ?;";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, after_clno);
			ps.setString(2, after_clname);
			ps.setString(3, after_dname);
			ps.setString(4, clno);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}

}
