package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import model.Doctor;
import utils.DBUtils;

public class DoctorDao {
	// 获取所有医生的信息，用ArrayList返回
	public ArrayList<Doctor> query_all_course() {
		Connection conn = DBUtils.getConnection();
		String sql = "select * from course order by cno;";
		ArrayList<Doctor> results = new ArrayList<Doctor>();
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Doctor temp = new Doctor();
				temp.setCno(rs.getString("Cno"));
				temp.setCname(rs.getString("Cname"));
				temp.setCteacher(rs.getString("Cteacher"));
				temp.setCcredit(rs.getInt("Ccredit"));
				results.add(temp);
			}
			// 关闭资源
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return results;
	}
	// 插入医生信息，返回一个int值表示状态,1：成功，0失败
	public int insert_course(String Cno, String Cname, String Cteacher, double Ccredit) {
		Connection conn = DBUtils.getConnection();
		String sql = "insert into course values(?,?,?,?);";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, Cno);
			ps.setString(2, Cname);
			ps.setString(3, Cteacher);
			ps.setDouble(4, Ccredit);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}
	// 删除医生信息，返回一个int值表示状态,1：成功，0失败
	public int delete_course(String Cno) {
		Connection conn = DBUtils.getConnection();
		String sql = "delete from course where Cno = ?;";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, Cno);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}
	//修改医生信息，返回一个int值表示状态,1：成功，0失败
	public int alter_course(String cno,String after_cno,String after_cname,String after_cteacher,double after_ccredit) {
		Connection conn = DBUtils.getConnection();
		String sql = "update course set cno = ?,cname = ?,cteacher = ?,ccredit = ? where cno = ?;";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, after_cno);
			ps.setString(2, after_cname);
			ps.setString(3, after_cteacher);
			ps.setDouble(4, after_ccredit);
			ps.setString(5, cno);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}

}

