package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import model.Operation;
import utils.DBUtils;

public class OperationDao {

	// 获取所有手术信息，用ArrayList返回
	public ArrayList<Operation> query_all_sc() {
		Connection conn = DBUtils.getConnection();
		String sql = "select student.sno sno,sname,ssex,sage,course.cteacher,cname,grade from sc,student,course where sc.sno = student.sno and course.cno = sc.cno order by sno;";
		ArrayList<Operation> results = new ArrayList<Operation>();
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Operation temp = new Operation();
				temp.setSno(rs.getString("sno"));
				temp.setSname(rs.getString("sname"));
				temp.setSsex(rs.getString("ssex"));
				temp.setSage(rs.getInt("sage"));
				temp.setCteacher(rs.getString("cteacher"));
				temp.setCname(rs.getString("cname"));
				temp.setGrade(rs.getString("grade"));
				results.add(temp);
			}
			// 关闭资源
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return results;
	}
	// 插入手术信息，返回一个int值表示状态,1：成功，0失败
	public int insert_sc(String Sno, String Cteacher, double Grade) {
		Connection conn = DBUtils.getConnection();
		String sql = "insert into sc values(?,?,?);";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, Sno);
			ps.setString(2, Cteacher);
			ps.setDouble(3, Grade);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}
	// 删除手术信息，返回一个int值表示状态,1：成功，0失败
	public int delete_sc(String Sno,String Cteacher) {
		Connection conn = DBUtils.getConnection();
		String sql = "delete from sc where sno = ? and cno = ?;";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setString(1, Sno);
			ps.setString(2, Cteacher);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}
	// 修改手术信息，返回一个int值表示状态,1：成功，0失败
	public int alter_sc(String Sno, String Cteacher,double after_grade) {
		Connection conn = DBUtils.getConnection();
		String sql = "update sc set grade = ? where sno = ? and cno = ?;";
		int flag = 0;
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ps.setDouble(1, after_grade);
			ps.setString(2, Sno);
			ps.setString(3, Cteacher);
			flag = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(conn);
		}
		return flag;
	}

}
