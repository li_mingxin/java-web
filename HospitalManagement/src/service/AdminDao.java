package service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RoomDao;
import dao.DoctorDao;
import dao.SectionDao;
import dao.OperationDao;
import dao.PatientDao;
import dao.UserDao;
import model.Room;
import model.Doctor;
import model.Section;
import model.Operation;
import model.Patient;
import model.User;


public class AdminDao extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private String action;//存储操作描述
	//接收请求
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		action = request.getParameter("action");
		//判断所执行操作
		switch (action) {
		//用户操作
		case "query_all_user":
			query_all_user(request, response);break;
		case "insert_user":
			insert_user(request,response);break;
		case "delete_user":
			delete_user(request, response);break;
		case "alter_user":
			alter_user(request, response);break;
		//系别操作
		case "query_all_department":
			query_all_department(request, response);break;
		case "insert_department":
			insert_department(request, response);break;
		case "delete_department":
			delete_department(request, response);break;
		case "alter_department":
			alter_department(request, response);break;
		//病房操作
		case "query_all_class":
			query_all_class(request, response);break;
		case "insert_class":
			insert_class(request, response);break;	
		case "delete_class":
			delete_class(request, response);break;
		case "alter_class":
			alter_class(request, response);break;
		//病人操作
		case "query_all_student":
			query_all_student(request, response);break;
		case "insert_student":
			insert_student(request, response);break;	
		case "delete_student":
			delete_student(request, response);break;
		case "alter_student":
			alter_student(request, response);break;
		//医生操作
		case "query_all_course":
			query_all_course(request, response);break;
		case "insert_course":
			insert_course(request, response);break;
		case "delete_course":
			delete_course(request, response);break;
		case "alter_course":
			alter_course(request, response);break;
		//手术操作
		case "query_all_sc":
			query_all_sc(request, response);break;
		case "insert_sc":
			insert_sc(request, response);break;
		case "delete_sc":
			delete_sc(request, response);break;
		case "alter_sc":
			alter_sc(request, response);break;
		default:
			break;
		}
	}
	/*-------------------------------- 用户 -----------------------------------*/
	//查询所有用户
	protected void query_all_user(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		UserDao userDao = new UserDao();
		
		ArrayList<User> results = userDao.query_all_user();
		PrintWriter out = response.getWriter();
		//输出结果
		if(results != null){
			out.write("<div class='all'>");
			out.write("<div><span>用户名</span><span>密码</span><span>权限级别</span></div>");
			for(User i: results){
				out.write("<div>");
				out.write("<span>"+i.getUsername()+"</span>");
				out.write("<span>"+i.getPassword()+"</span>");
				out.write("<span>"+i.getLevel()+"</span>");
				out.write("</div>");
			}
			out.write("</div>");
		}
		
		out.flush();
		out.close();
	}
	//插入用户
	protected void insert_user(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String level = request.getParameter("level");
		
		int flag =  new UserDao().insert_user(username, password, level);
		String info = null;
		PrintWriter out =  response.getWriter();
		if(flag == 1){
			info = "用户插入成功！";
		}else{
			info = "错误：用户插入失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>"+info+"</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	//删除用户
	protected void delete_user(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String username = request.getParameter("username");
		
		int flag =  new UserDao().delete_user(username);
		String info = null;
		PrintWriter out =  response.getWriter();
		if(flag == 1){
			info = "成功删除名为"+username+"用户！";
		}else{
			info = "错误：删除用户失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>"+info+"</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	//修改用户
	protected void alter_user(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String username = request.getParameter("username");
		String after_username = request.getParameter("after_username");
		String after_password = request.getParameter("after_password");
		String after_level = request.getParameter("after_level");
		
		int flag =  new UserDao().alter_user(username,after_username,after_password,after_level);
		String info = null;
		PrintWriter out =  response.getWriter();
		if(flag == 1){
			info = "名为"+username+"用户信息修改成功！";
		}else{
			info = "错误：修改用户失败!";
		}
		out.write("<div class='error'>");
		out.write("<div>"+info+"</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	
	/*-------------------------------- 院系-----------------------------------*/
	// 查询所有院系
	protected void query_all_department(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");

		ArrayList<Section> results = new SectionDao().query_all_department();
		PrintWriter out = response.getWriter();
		// 输出结果
		if (results != null) {
			out.write("<div class='all'>");
			out.write("<div><span>系编号</span><span>系名</span></div>");
			for (Section i : results) {
				out.write("<div>");
				out.write("<span>" + i.getDno() + "</span>");
				out.write("<span>" + i.getDname() + "</span>");
				out.write("</div>");
			}
			out.write("</div>");
		}
		out.flush();
		out.close();
	}
	// 插入院系
	protected void insert_department(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String dno = request.getParameter("dno");
		String dname = request.getParameter("dname");
		int flag = new SectionDao().insert_department(dno, dname);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "院系"+dname+"插入成功！";
		} else {
			info = "错误：院系插入失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 删除院系
	protected void delete_department(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String dno = request.getParameter("dno");
		int flag = new SectionDao().delete_department(dno);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "成功删除" + dno + "号院系！";
		} else {
			info = "错误：删除院系失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 修改院系
	protected void alter_department(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String dno = request.getParameter("dno");
		String after_dno = request.getParameter("after_dno");
		String after_dname = request.getParameter("after_dname");
		int flag = new SectionDao().alter_department(dno, after_dno, after_dname);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = dno + "号系修改成功！";
		} else {
			info = "错误：修改院系失败!";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}

	/*-------------------------------- 病房-----------------------------------*/
	// 查询所有病房
	protected void query_all_class(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		ArrayList<Room> results = new RoomDao().query_all_class();
		PrintWriter out = response.getWriter();
		//输出结果
		if (results != null) {
			out.write("<div class='all'>");
			out.write("<div><span>病房编号</span><span>病房名</span><span>所属科系</span></div>");
			for (Room i : results) {
				out.write("<div>");
				out.write("<span>" + i.getClno() + "</span>");
				out.write("<span>" + i.getClname() + "</span>");
				out.write("<span>" + i.getDname() + "</span>");
				out.write("</div>");
			}
			out.write("</div>");
		}
		out.flush();
		out.close();
	}
	// 插入病房
	protected void insert_class(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String clno = request.getParameter("clno");
		String clname = request.getParameter("clname");
		String dname = request.getParameter("dname");
		int flag = new RoomDao().insert_class(clno, clname, dname);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "病房"+clname+"插入成功！";
		} else {
			info = "错误：病房插入失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 删除病房
	protected void delete_class(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String clno = request.getParameter("clno");
		int flag = new RoomDao().delete_class(clno);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "成功删除" + clno + "病房！";
		} else {
			info = "错误：删除病房失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 修改病房
	protected void alter_class(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String clno = request.getParameter("clno");
		String after_clno = request.getParameter("after_clno");
		String after_clname = request.getParameter("after_clname");
		String after_dname = request.getParameter("after_dname");
		int flag = new RoomDao().alter_class(clno, after_clno, after_clname, after_dname);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "病房"+clno+"修改成功！";
		} else {
			info = "错误：修改病房失败!";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}

	/*-------------------------------- 病人-----------------------------------*/
	// 查询所有病人
	protected void query_all_student(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		ArrayList<Patient> results = new PatientDao().query_all_student();
		PrintWriter out = response.getWriter();
		// 输出结果
		if (results != null) {
			out.write("<div class='all'>");
			out.write("<div><span>病人号</span><span>姓名</span><span>性别</span><span>年龄</span><span>所在病房</span></div>");
			for (Patient i : results) {
				out.write("<div>");
				out.write("<span>" + i.getSno() + "</span>");
				out.write("<span>" + i.getSname() + "</span>");
				out.write("<span>" + i.getSsex() + "</span>");
				out.write("<span>" + i.getSage() + "</span>");
				out.write("<span>" + i.getClname() + "</span>");
				out.write("</div>");
			}
			out.write("</div>");
		}
		out.flush();
		out.close();
	}
	// 插入病人
	protected void insert_student(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String sno = request.getParameter("sno");
		String sname = request.getParameter("sname");
		String ssex = request.getParameter("ssex");
		int sage = Integer.parseInt(request.getParameter("sage"));
		String clno = request.getParameter("clno");
		int flag = new PatientDao().insert_student(sno, sname, ssex, sage, clno);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "病人"+sname+"插入成功！";
		} else {
			info = "错误：病人插入失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 删除病人
	protected void delete_student(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String sno = request.getParameter("sno");
		int flag = new PatientDao().delete_student(sno);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "成功删除" + sno + "号病人！";
		} else {
			info = "错误：删除病人失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 修改病人信息
	protected void alter_student(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String sno = request.getParameter("sno");
		String after_sno = request.getParameter("after_sno");
		String after_sname = request.getParameter("after_sname");
		String after_ssex = request.getParameter("after_ssex");
		int after_sage = Integer.parseInt(request.getParameter("after_sage"));
		String after_clname = request.getParameter("after_clname");
		int flag = new PatientDao().alter_class(sno, after_sno, after_sname, after_ssex, after_sage, after_clname);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "病人"+sno+"信息修改成功！";
		} else {
			info = "病人王大爷修改成功";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}

	/*-------------------------------- 医生 -----------------------------------*/
	//查询所有医生
	protected void query_all_course(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		ArrayList<Doctor> results = new DoctorDao().query_all_course();
		PrintWriter out = response.getWriter();
		if(results != null){
			//输出结果
			if(results != null){
				out.write("<div class='all'>");
				out.write("<div><span>医生号</span><span>类型名称</span><span>医生姓名</span><span>挂号费</span></div>");
				for(Doctor i:results) {
					out.write("<div>");
					out.write("<span>"+i.getCno()+"</span>");
					out.write("<span>"+i.getCname()+"</span>");
					out.write("<span>"+i.getCteacher()+"</span>");
					out.write("<span>"+i.getCcredit()+"</span>");
					out.write("</div>");
				}
				out.write("</div>");
			}
		}
		out.flush();
		out.close();
	}
	//插入医生
	protected void insert_course(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String Cno = request.getParameter("cno");
		String Cname = request.getParameter("cname");
		String Cteacher = request.getParameter("cteacher");
		int Ccredit = Integer.parseInt(request.getParameter("ccredit"));
		int flag =  new DoctorDao().insert_course(Cno, Cname, Cteacher, Ccredit);
		String info = null;
		PrintWriter out =  response.getWriter();
		if(flag == 1){
			info = "医生"+Cname+"插入成功！";
		}else{
			info = "错误：医生插入失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>"+info+"</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	//删除医生
	protected void delete_course(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String cno = request.getParameter("cno");
		int flag =  new DoctorDao().delete_course(cno);
		String info = null;
		PrintWriter out =  response.getWriter();
		if(flag == 1){
			info = "成功删除"+cno+"医生！";
		}else{
			info = "错误：删除医生失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>"+info+"</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	//修改医生信息
	protected void alter_course(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		
		String cno = request.getParameter("cno");
		String after_cno = request.getParameter("after_cno");
		String after_cname = request.getParameter("after_cname");
		String after_cteacher = request.getParameter("after_cteacher");
		double after_ccredit = Double.parseDouble(request.getParameter("after_ccredit"));
		int flag = new DoctorDao().alter_course(cno, after_cno, after_cname, after_cteacher, after_ccredit);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = cno + "号医生修改成功！";
		} else {
			info = "错误：修改医生信息失败!";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}

	/*-------------------------------- 病人手术-----------------------------------*/
	// 查询所有手术安排
	protected void query_all_sc(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		ArrayList<Operation> results = new OperationDao().query_all_sc();
		PrintWriter out = response.getWriter();
		// 输出结果
		if (results != null) {
			out.write("<div id='all_sc' class='all'>");
			out.write("<div><span>病人号</span><span>姓名</span><span>性别</span><span>年龄</span><span>医生名</span><span>医生类型</span><span>手术时间</span></div>");
			for (Operation i : results) {
				out.write("<div>");
				out.write("<span>" + i.getSno() + "</span>");
				out.write("<span>" + i.getSname() + "</span>");
				out.write("<span>" + i.getSsex() + "</span>");
				out.write("<span>" + i.getSage() + "</span>");
				out.write("<span>" + i.getCteacher() + "</span>");
				out.write("<span>" + i.getCname() + "</span>");
				out.write("<span>" + i.getGrade() + "</span>");
				out.write("</div>");
			}
			out.write("</div>");
		}
		out.flush();
		out.close();
	}
	// 插入一条手术记录
	protected void insert_sc(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String sno = request.getParameter("sno");
		String cteacher = request.getParameter("cteacher");
		double grade = Double.parseDouble(request.getParameter("grade"));
		int flag = new OperationDao().insert_sc(sno, cteacher, grade);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = sno + "号病人" + cteacher + "手术信息"+grade+"插入成功！";
		} else {
			info = "错误：手术信息插入失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 删除手术记录
	protected void delete_sc(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String sno = request.getParameter("sno");
		String cteacher = request.getParameter("cno");
		int flag = new OperationDao().delete_sc(sno, cteacher);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = "成功删除" + sno + "号病人"+cteacher+"手术信息！";
		} else {
			info = "错误：删除手术信息失败！";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}
	// 修改手术记录
	protected void alter_sc(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String sno = request.getParameter("sno");
		String cteacher = request.getParameter("cteacher");
		double after_grade = Double.parseDouble(request.getParameter("after_grade"));
		int flag = new OperationDao().alter_sc(sno, cteacher, after_grade);
		String info = null;
		PrintWriter out = response.getWriter();
		if (flag == 1) {
			info = sno + "号病人" + cteacher + "号手术记录修改成功！";
		} else {
			info = "错误：修改手术记录失败!";
		}
		out.write("<div class='error'>");
		out.write("<div>" + info + "</div>");
		out.write("</div>");
		out.flush();
		out.close();
	}

}
