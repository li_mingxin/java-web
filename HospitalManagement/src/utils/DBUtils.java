package utils;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class DBUtils {
	
    public static Connection getConnection(){
    	//数据库url、用户名、密码
    	String dbUserName = "root";
    	String dbUserPasswd = "123456";
    	String dbURL = "jdbc:mysql://localhost:3306/studentinfomanagement?"
    	            + "user="+dbUserName+"&password="+dbUserPasswd+"&useUnicode=true&characterEncoding=UTF8";
    	Connection conn = null;
    	try {
    		//1、注册JDBC驱动
    		Class.forName("com.mysql.jdbc.Driver");
    		//2、获取数据库连接
    		conn = (Connection) DriverManager.getConnection(dbURL,dbUserName,dbUserPasswd);
    	} catch (ClassNotFoundException | SQLException e) {
    		e.printStackTrace();
    	}
    	return conn;
    }


    public static void closeConnection(Connection conn) {
		
    	if(conn != null){
    		try {
    			//关闭数据库连接
				conn.close();
			} catch (SQLException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
    	}
	}
}
