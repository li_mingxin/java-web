package model;

import java.io.Serializable;

public class Patient implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String Sno;//病人号
	private String Sname;//姓名
	private String Ssex;//性别
	private int Sage;//年龄
	private String Clname;//所在病房
	
	public String getSno() {
		return Sno;
	}
	public void setSno(String sno) {
		Sno = sno;
	}
	public String getSname() {
		return Sname;
	}
	public void setSname(String sname) {
		Sname = sname;
	}
	public String getSsex() {
		return Ssex;
	}
	public void setSsex(String ssex) {
		Ssex = ssex;
	}
	public int getSage() {
		return Sage;
	}
	public void setSage(int sage) {
		Sage = sage;
	}
	public String getClname() {
		return Clname;
	}
	public void setClname(String clname) {
		this.Clname = clname;
	}
	
	
}
