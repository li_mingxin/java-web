package model;

import java.io.Serializable;

public class Operation implements Serializable {
			
	private static final long serialVersionUID = 1L;
	private String Sno;//病人号
	private String Sname;//姓名
	private String Ssex;//性别
	private int Sage;//年龄
	private String Cteacher;//医生名
	private String Cname;//医生类型
	private String Grade;//手术时间

	public String getSno() {
		return Sno;
	}
	public void setSno(String sno) {
		Sno = sno;
	}
	public String getSname() {
		return Sname;
	}
	public void setSname(String sname) {
		Sname = sname;
	}
	public String getSsex() {
		return Ssex;
	}
	public void setSsex(String ssex) {
		Ssex = ssex;
	}
	public int getSage() {
		return Sage;
	}
	public void setSage(int sage) {
		Sage = sage;
	}
	public String getCteacher() {
		return Cteacher;
	}
	public void setCteacher(String cteacher) {
		Cteacher = cteacher;
	}
	public String getCname() {
		return Cname;
	}
	public void setCname(String cname) {
		Cname = cname;
	}
	public String getGrade() {
		return Grade;
	}
	public void setGrade(String grade) {
		Grade = grade;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
}
