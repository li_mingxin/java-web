package model;

import java.io.Serializable;

public class Room implements Serializable {

	private static final long serialVersionUID = 1L;

	private String Clno;//病房编号
	private String Clname;//病房名称
	private String Dname;//所属科系
	public String getClno() {
		return Clno;
	}
	public void setClno(String clno) {
		Clno = clno;
	}
	public String getClname() {
		return Clname;
	}
	public void setClname(String clname) {
		Clname = clname;
	}
	public String getDname() {
		return Dname;
	}
	public void setDname(String dname) {
		Dname = dname;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
