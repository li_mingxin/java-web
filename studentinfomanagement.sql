/*
 Navicat Premium Data Transfer

 Source Server         : LMX
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : studentinfomanagement

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 01/07/2021 16:25:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `Clno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Clname` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Dno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Clno`) USING BTREE,
  INDEX `FK_Class_Department`(`Dno`) USING BTREE,
  CONSTRAINT `class_ibfk_1` FOREIGN KEY (`Dno`) REFERENCES `department` (`Dno`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES ('0001', '麻醉室', '1001');
INSERT INTO `class` VALUES ('0002', '换药室', '1002');
INSERT INTO `class` VALUES ('0003', '手术室', '1003');
INSERT INTO `class` VALUES ('0004', '急救室', '1004');
INSERT INTO `class` VALUES ('0005', '消毒室', '1005');
INSERT INTO `class` VALUES ('0006', '配药室', '1002');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `Cno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Cname` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Cteacher` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Ccredit` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`Cno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '普通', '张医生', 20);
INSERT INTO `course` VALUES ('2', '急诊', '李医生', 50);
INSERT INTO `course` VALUES ('3', '专家', '赵医生', 100);
INSERT INTO `course` VALUES ('4', '名医', '王医生', 200);
INSERT INTO `course` VALUES ('6', '名医', '曲医生', 150);

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `Dno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Dname` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Dno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1001', '骨科');
INSERT INTO `department` VALUES ('1002', '外科');
INSERT INTO `department` VALUES ('1003', '内科');
INSERT INTO `department` VALUES ('1004', '眼科');
INSERT INTO `department` VALUES ('1005', '儿科');
INSERT INTO `department` VALUES ('1006', '鼻科');

-- ----------------------------
-- Table structure for sc
-- ----------------------------
DROP TABLE IF EXISTS `sc`;
CREATE TABLE `sc`  (
  `Sno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Cno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Grade` date NOT NULL,
  PRIMARY KEY (`Sno`, `Cno`) USING BTREE,
  INDEX `FK_SC_Course`(`Cno`) USING BTREE,
  CONSTRAINT `sc_ibfk_1` FOREIGN KEY (`Sno`) REFERENCES `student` (`Sno`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sc_ibfk_2` FOREIGN KEY (`Cno`) REFERENCES `course` (`Cno`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc
-- ----------------------------
INSERT INTO `sc` VALUES ('2001', '1', '2021-06-16');
INSERT INTO `sc` VALUES ('2002', '2', '2021-06-27');
INSERT INTO `sc` VALUES ('2003', '4', '2021-06-06');
INSERT INTO `sc` VALUES ('2004', '2', '2021-06-18');
INSERT INTO `sc` VALUES ('2004', '3', '2021-06-15');
INSERT INTO `sc` VALUES ('2005', '1', '2021-06-21');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `Sno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Sname` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Ssex` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Sage` smallint(6) NULL DEFAULT NULL,
  `Clno` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Sno`) USING BTREE,
  INDEX `FK_Student_Class`(`Clno`) USING BTREE,
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`Clno`) REFERENCES `class` (`Clno`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('2001', '张明', '男', 20, '0001');
INSERT INTO `student` VALUES ('2002', '秦羽', '女', 19, '0002');
INSERT INTO `student` VALUES ('2003', '刘翔', '男', 19, '0003');
INSERT INTO `student` VALUES ('2004', '李二', '男', 44, '0004');
INSERT INTO `student` VALUES ('2005', '王五', '女', 55, '0005');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `username` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `level` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('admin', '666', '管理员');
INSERT INTO `user` VALUES ('user', '123456', '用户');

SET FOREIGN_KEY_CHECKS = 1;
